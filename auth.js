const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;

//function for creating token for the user that logged in on the app
module.exports.createAccessToken = user => {
	//token payload
	const data = {
		id: user._id
	}

	return jwt.sign(data, secret, {});
}

//function for verifying the user
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			return (error) ? res.send({auth: 'failed'}) : next()
		});
	}

	else res.send({auth: 'failed'});
}

//function for decoding the user's token to retrieve the payload 
module.exports.decode = token => {
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.decode(token, secret, (error, data) => {
			return (error) ? null : jwt.decode(token, {complete: true}).payload 
		});
	}

	else return null
}