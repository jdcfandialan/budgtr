const Post = require('../models/Post');
const PostType = require('../models/PostType');
const PostCategory = require('../models/PostCategory');
const Account = require('../models/Account');
const e = require('express');

//function for creating a new post type
module.exports.createNewPostType = params => {
	const postType = new PostType ({
		typeName: params.typeName
	});

	return postType.save().then((postType, error) => {
		return (error) ? false : true
	});
}

//function for creating a new post category
module.exports.createNewPostCategory = params => {
	const postCategory = new PostCategory({
		categoryName: params.categoryName
	});

	return postCategory.save().then((postCategory, error) => {
		return (error) ? false : true
	});
}

//function for getting all the post types
module.exports.getAllTypes = () => {
	return PostType.find({}).then(postTypes => postTypes);
}

//function for getting all the post categories
module.exports.getAllCategories = () => {
	return PostCategory.find({}).then(categories => categories);
}

//function for creating a new post
module.exports.createPost = params => {
	const post = new Post({
		postName: params.postName,
		amount: params.amount,
		postTypeId: params.postTypeId,
		postCategoryId: params.postCategoryId,
		accountId: params.accountId,
		userId: params.userId
	});

	return post.save().then((post, error) => {
		if(error) return false

		else{
			return Account.findById(params.accountId).then((account, error) => {
				if(error) return false

				else{
					account.posts.push({
						postId: post._id
					})

					return account.save().then((updatedAccount, error) => {
						return (error) ? false : true
					})
				}
			})
		}
	});
}

//function for retrieving all the posts by a specific user
module.exports.getAllPostsByUser = params => {
	return Post.find({
		userId: params.userId
	}).then(posts => posts);
}

//function for retrieving all the posts of the same type of the current user
module.exports.findByType = params => {
	return Post.find({
		postTypeId: params.postTypeId,
		userId: params.userId
	}).then((filteredPosts, error) => {
		return (error) ? false : filteredPosts
	});
}

//function for retrieving all the posts of the same category of the current user
module.exports.findByCategory = params => {
	return Post.find({
		postCategoryId: params.postCategoryId,
		userId: params.userId
	}).then((filteredPosts, error) => {
		return (error) ? false : filteredPosts
	});
}

//function for deleting a specific post
module.exports.deletePost = params => {
	return Account.findById(params.accountId).then((account, error) => {
		if(error) return false
		
		else{
			const newArray = account.posts.filter(post => params.postId !== post.postId)
			account.posts = newArray;

			return account.save().then((updatedAccount, error) => {
				if(error) return false

				else{
					return Post.deleteOne({_id: params.postId}).then((updatedPost, error) => {
						if(error) return false
						else return true
					})
				}
			})
		}
	})
}

//function for updating a specific post
module.exports.updatePost = params => {
	return Post.findById(params.id).then((post, error) => {
		if(error) return false

		else{
			//assign the new values
			post.postName = params.postName;
			post.amount = params.amount;
			post.postTypeId = params.postTypeId;
			post.postCategoryId = params.postCategoryId;
			post.accountId = params.accountId;
			post.userId = params.userId;

			return post.save().then((updatedPost, error) => {
				return (error) ? false : true
			});
		}
	});
}

module.exports.findPostsByAccount = params => {
	return Post.find({
		accountId: params
	}).sort({createdOn: 'descending'}).then(posts => posts)
}