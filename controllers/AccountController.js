const Account = require('../models/Account');
const AccountType = require('../models/AccountType');
const AccountCurrency = require('../models/AccountCurrency');
const User = require('../models/User');
const Post = require('../models/Post');

//function for creating a new account type
module.exports.createNewAccountType = params => {
	const accountType = new AccountType ({
		typeName: params.typeName
	});

	return AccountType.find({typeName: accountType.typeName}).then(filteredAccount => {
		if(filteredAccount.length > 0) return false

		else{
			return accountType.save().then((accountType, error) => {
				return (error) ? false : true
			});
		}
	})
}

//function for creating a new currency type
module.exports.createNewAccountCurrency = params => {
	const accountCurrency = new AccountCurrency ({
		currencyAbb: params.currencyAbb
	});

	return AccountCurrency.find({currencyAbb: accountCurrency.currencyAbb}).then(filteredAccount => {
		if(filteredAccount.length > 0) return false

		else {
			return accountCurrency.save().then((accountCurrency, error) => {
				return (error) ? false : true
			});
		}
	})
}

//function for retrieving all the account types
module.exports.getAllTypes = () => {
	return AccountType.find({}).then(accountTypes => accountTypes);
}

//function for retrieving all the account currencies
module.exports.getAllCurrencies = () => {
	return AccountCurrency.find({}).then(accountCurrencies => accountCurrencies);
}

//function for creating a new account
module.exports.createAccount = params => {
	const account = new Account ({
		bankName: params.bankName,
		amount: params.amount,
		accountTypeId: params.accountTypeId,
		accountCurrencyId: params.accountCurrencyId,
		userId: params.userId
	});
	
	//save the newly account in the database
	return account.save().then((account, error) => {
		if(error) return false
		
		//process for pushing the newly created account into the user
		else{
			//find the user record in the database
			return User.findById(params.userId).then((user, error) => {
				if(error) return false
				
				//push the new account id in the user's array of account IDs
				else{
					user.accounts.push({
						accountId: account._id
					});

					return user.save().then((updatedUser, error) => {
						return (error) ? false : true
					});
				}
			})
		}
	});
}

//function for retrieving all the accounts of the user
module.exports.getAccounts = params => {
	return Account.find({userId: params.userId}).then(accounts => accounts);
}

//function for retrieving all of the accounts
module.exports.getAll = () => {
	return Account.find({}).then(accounts => accounts);
}

//function for finding a single account
module.exports.findAccount = params => {
	return Account.findById(params.id).then((account, error) => {
		return (error) ? false : account
	});
}

//function for retrieving all the accounts of the same type of a specific user
module.exports.findByType = params => {
	//retrieve all the accounts of the user by type
	return Account.find({
		userId: params.userId,
		accountTypeId: params.accountTypeId
	}).then((filteredAccounts, error) => {
		return (error) ? false : filteredAccounts
	});
}

//function for retrieving all the accounts of the same currency of a specific user
module.exports.findByCurrency = params => {
	//retrieve all the accounts of the user by currency
	return Account.find({
		userId: params.userId,
		accountCurrencyId: params.accountCurrencyId
	}).then((filteredAccounts, error) => {
		return (error) ? false : filteredAccounts
	});
}

//function for deleting an account
module.exports.deleteAccount = params => {
	//look for the user document in the database
	return User.findById(params.userId).then((user, error) => {
		if (error) return false

		else{
			//filter out the account ID to be deleted from the user's array of account IDs
			const newAccountsArray = user.accounts.filter(account => params.accountId === account.accountId);
			
			user.accounts = newAccountsArray;
			
			//update the user document
			return user.save().then((updatedUser, error) => {
				if(error) return false

				else{
					//delete all the posts related to the account to be deleted
					return Post.deleteMany({accountId: params.accountId}).then((posts, error) => {
						if(error) return false

						else{
							//delete the account from the database
							return Account.deleteOne({_id: params.accountId}).then((updatedAccount, error) => {
								return (error) ? false : true
							});	
						}
					});
				}
			});
		}
	});
}

//function for updating an account
module.exports.updateAccount = params => {
	return Account.findById(params.id).then((account, error) => {
		if(error) return false

		else {
			account.bankName = params.bankName;
			account.amount = params.amount;
			account.accountTypeId = params.accountTypeId;
			account.accountCurrencyId = params.accountCurrencyId;
			account.userId = params.userId;
			
			return account.save().then((updatedAccount, error) => {
				return (error) ? false : true
			})
		}
	})
}