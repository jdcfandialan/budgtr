const User = require('../models/User');
const bcrypt = require('bcrypt');
const {OAuth2Client} = require('google-auth-library');
const auth = require('../auth');
const clientId = '192671143805-5269mmsqghgmvr4ni6u217djtuja5c0m.apps.googleusercontent.com';


//function for creating a new user record
module.exports.register = params => {
	const user = new User ({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'default'
	});

	//check if the email provided already exists
	return User.find({email: user.email}).then(filteredUser => {
		if(filteredUser.length > 0) return false

		else{
			return user.save().then((newUser, error) => {
				return (error) ? false : true
			});
		}
	});
}

//function for logging into the app
module.exports.login = params => {
	return User.findOne({email: params.email}).then(user => {
		//case if the user does not exist
		if(user === null) return false

		else{
			const isPasswordMatched = bcrypt.compareSync(params.password, user.password);
			
			//case if user input password matches the user's password on the database
			if(isPasswordMatched) return {
				accessToken: auth.createAccessToken(user.toObject())
			}

			else return false
		}
	});
}

//function for retrieving the current user's details
module.exports.getDetails = params => {
	return User.findById(params.userId).then((user, error) => {
		if(error) return false

		else{
			//set the user's password to undefined so it will not be accessible
			user.password = undefined;
			return user
		}
	});
}

module.exports.verifyGoogleTokenId = async tokenId => {
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({idToken: tokenId , audience: clientId});

	if(data.payload.email_verified){
		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null){
			if(user.loginType === 'google'){
				return {accessToken: auth.createAccessToken(user.toObject())}
			}

			else{
				return {error: 'login-type-error'}
			}
		}

		else{
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			});

			return newUser.save().then((user, error) => {
				//console.log(user);
				return (error) ? false : {accessToken: auth.createAccessToken(user.toObject())}
			});
		}
	}

	else{
		return {error: 'google-auth-error'}
	}
}