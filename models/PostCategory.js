const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostCategorySchema = new Schema (
	{
		categoryName: {
			type: String,
			required: [true, 'Post Category Name is required!']
		}
	}
);

module.exports = mongoose.model('PostCategory', PostCategorySchema);