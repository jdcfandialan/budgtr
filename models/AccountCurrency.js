const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountCurrencySchema = new Schema (
	{
		currencyAbb: {
			type: String,
			required: [true, 'Currency Abrreviation is required!']
		}
	}
);

module.exports = mongoose.model('AccountCurrency', AccountCurrencySchema);