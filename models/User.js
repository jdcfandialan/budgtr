const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema (
	{
		firstName: {
			type: String,
			required: [true, 'First Name is required!']
		},

		lastName: {
			type: String,
			required: [true, 'Last Name is required!']
		},

		email: {
			type: String,
			required: [true, 'Email is required!']
		},

		password: {
			type: String
		},

		loginType: {
			type: String,
			required: [true, 'Login Type is required!']
		},

		accounts: [
			{
				accountId: {
					type: String,
					required: [true, 'Account ID is required!']
				}
			}
		]
	}
);

module.exports = mongoose.model('User', UserSchema);