const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema (
	{
		bankName: {
			type: String,
			required: [true, 'Bank Name is required!']
		},
		
		amount: {
			type: Number,
			default: 0
		},

		accountTypeId: {
			type: String,
			required: [true, 'Account Type ID is required!']
		},

		accountCurrencyId: {
			type: String,
			required: [true, 'Account Currency ID is required!']
		},

		userId: {
			type: String,
			required: [true, 'User ID is required!']
		},

		posts : [
			{
				postId: {
					type: String,
					required: [true, 'Post ID is required!']
				}
			}
		]
	}
);

module.exports = mongoose.model('Account', AccountSchema);