const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema (
	{
		postName: {
			type: String
		},

		amount: {
			type: Number,
			required: [true, 'Post Amount is required!']
		},

		postTypeId: {
			type: String,
			required: [true, 'Post Type ID is required!']
		},

		postCategoryId: {
			type: String,
			required: [true, 'Post Category ID is required!']
		},

		accountId: {
			type: String,
			required: [true, 'Account ID is required!']
		},

		userId: {
			type: String,
			required: [true, 'User ID is required!']
		},

		createdOn: {
			type: Date,
			default: Date.now
		}
	}
);

module.exports = mongoose.model('Post', PostSchema);