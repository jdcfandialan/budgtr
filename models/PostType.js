const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostTypeSchema = new Schema (
	{
		typeName: {
			type: String,
			required: [true, 'Post Type Name is required!']
		}
	}
);

module.exports = mongoose.model('PostType', PostTypeSchema);