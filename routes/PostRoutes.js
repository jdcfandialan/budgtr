const express = require('express');
const router = express.Router();

const PostController = require('../controllers/PostController');
const auth = require('../auth');

//route for creating a new post type
router.post('/createNewPostType', (req, res) => {
	PostController.createNewPostType(req.body).then(result => res.send(result));
});

//route for creating a new post category
router.post('/createNewPostCategory', (req, res) => {
	PostController.createNewPostCategory(req.body).then(result => res.send(result));
});

//route for retrieving all the post types
router.get('/postTypes', (req, res) => {
	PostController.getAllTypes().then(result => res.send(result));
});

//route for retrieving all the post categories
router.get('/postCategories', (req, res) => {
	PostController.getAllCategories().then(result => res.send(result));
});

//route for creating a new post
router.post('/newPost', auth.verify, (req, res) => {
	const params = {
		postName: req.body.postName,
		amount: req.body.amount,
		postTypeId: req.body.postTypeId,
		postCategoryId: req.body.postCategoryId,
		accountId: req.body.accountId,
		userId: auth.decode(req.headers.authorization).id
	}

	PostController.createPost(params).then(result => res.send(result));
});

//route for retrieving all the posts of the current user
router.get('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id
	}

	PostController.getAllPostsByUser(params).then(result => res.send(result));
});

//route for retrieving all the posts of the same type of the current user
router.get('/types/:typeId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		postTypeId: req.params.typeId
	}

	PostController.findByType(params).then(result => res.send(result));
});

//route for retrieving all the posts of the same category of the current user
router.get('/categories/:categoryId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		postCategoryId: req.params.categoryId
	}

	PostController.findByCategory(params).then(result => res.send(result));
});

//router for deleting a post
router.delete('/:accountId/:postId', (req, res) => {
	const params = {
		accountId: req.params.accountId,
		postId: req.params.postId
	}

	PostController.deletePost(params).then(result => res.send(result));
});

//route for updating a post
router.put('/:postId', auth.verify, (req, res) => {
	const params = {
		id: req.params.postId,
		postName: req.body.postName,
		amount: req.body.amount,
		postTypeId: req.body.postTypeId,
		postCategoryId: req.body.postCategoryId,
		accountId: req.body.accountId,
		userId: auth.decode(req.headers.authorization).id
	}

	PostController.updatePost(params).then(result => res.send(result));
});

router.get('/:accountId', (req, res) => {
	PostController.findPostsByAccount(req.params.accountId).then(result => res.send(result));
})

module.exports = router;