const express = require('express');
const router = express.Router();

const AccountController = require('../controllers/AccountController');
const auth = require('../auth');

//route for creating a new account type
router.post('/createNewAccountType', (req, res) => {
	AccountController.createNewAccountType(req.body).then(result => res.send(result));
});

//route for creating a new account currency
router.post('/createNewAccountCurrency', (req, res) => {
	AccountController.createNewAccountCurrency(req.body).then(result => res.send(result));
});

//route for retrieving all the account types
router.get('/accountTypes', (req, res) => {
	AccountController.getAllTypes().then(result => res.send(result));
});

//route for retrieving all the account currencies
router.get('/accountCurrencies', (req, res) => {
	AccountController.getAllCurrencies().then(result => res.send(result));
});

//route for creating a new account
router.post('/newAccount', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		bankName: req.body.bankName,
		amount: req.body.amount,
		accountTypeId: req.body.accountTypeId,
		accountCurrencyId: req.body.accountCurrencyId
	}

	AccountController.createAccount(params).then(result => res.send(result));
});

//route for getting all of the accounts
router.get('/getAll', (req, res) => {
	AccountController.getAll().then(accounts => res.send(accounts));
})

//route for retrieving all the accounts of the user
router.get('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id
	}

	AccountController.getAccounts(params).then(result => res.send(result));
});

//route for retrieving a single account
router.get('/:id', (req, res) => {
	AccountController.findAccount({id: req.params.id}).then(result => res.send(result));
});

//route for retrieving all the user's accounts of the same type
router.get('/types/:typeId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		accountTypeId: req.params.typeId
	}

	AccountController.findByType(params).then(result => res.send(result));
});

//route for retrieving all the user's accounts of the same currency
router.get('/currencies/:currencyId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		accountCurrencyId: req.params.currencyId
	}

	AccountController.findByCurrency(params).then(result => res.send(result));
});

//route for deleting an account
router.delete('/:accountId', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		accountId: req.params.accountId
	}

	AccountController.deleteAccount(params).then(result => res.send(result));
});

//route for updating an account
router.put('/:id', auth.verify, (req, res) => {
	const params = {
		id: req.params.id,
		userId: auth.decode(req.headers.authorization).id,
		bankName: req.body.bankName,
		amount: req.body.amount,
		accountTypeId: req.body.accountTypeId,
		accountCurrencyId: req.body.accountCurrencyId
	}

	AccountController.updateAccount(params).then(result => res.send(result))
})

module.exports = router;