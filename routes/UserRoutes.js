const express = require('express');
const router = express.Router();

const auth = require('../auth');
const UserController = require('../controllers/UserController');

//route for creating a new user
router.post('/', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
});

//route for user login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result));
});

//route for retrieving the current user's details
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	UserController.getDetails({userId: user.id}).then(result => res.send(result));
});

router.post('/verifyGoogleIdToken', (req, res) => {
	UserController.verifyGoogleTokenId(req.body.tokenId).then(result => res.send(result));
});

module.exports = router;