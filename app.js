const express = require('express');
const mongoose = require('mongoose');
const env = require('dotenv');
const cors = require('cors');

const db = mongoose.connection;
const Schema = mongoose.Schema;

const app = express();

env.config();

const corsOptions = {
  origin: ['https://budgtr.vercel.app', 'http://localhost:3000'],
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

mongoose.connect(process.env.DB_MONGO, {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

db.on('error',() => {
	console.error(`'Error on database ${process.env.DB_MONGO} connection!'`)
});

db.once('open',() => {
	console.log(`connected to cloud database!`)
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const userRoutes = require('./routes/UserRoutes');
app.use('/api/users', cors(corsOptions), userRoutes);

const accountRoutes = require('./routes/AccountRoutes');
app.use('/api/accounts', cors(corsOptions), accountRoutes);

const postRoutes = require('./routes/PostRoutes');
app.use('/api/posts', cors(corsOptions), postRoutes);

app.listen(process.env.PORT, ()=>{
	console.log(`Now listening on PORT ${process.env.PORT}`);
});